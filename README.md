# Introduction to Web Design & Programming
This is a sample project of Django Framework.

## Contents (branches)
* **master** - Django Starter
* **step-1** - Connect Django with HTML
* **step-2** - Add CSS
* **step-3** - Deploy to Heroku

## Local Setup
* Fork this repository, so you can make this repository your own.

* Clone your forked repository
```cmd
git clone https://gitlab.com/<yourGitLabUsername>/django-workshop.git
```

* Go inside the sample project
```cmd
cd django-workshop
```

* Create a virtual environment
```cmd
python -m venv env
```

* Activate the virtual environment (Windows)
```cmd
cd env/Scripts
activate.bat
cd ../..
```

* Activate the virtual environment (Mac / Linux)
```cmd
source env/bin/activate
```

* Install dependencies
```cmd
pip install -r requirements.txt
```

* Make migration for database
```cmd
python manage.py makemigrations
python manage.py migrate
```

* Run the server
```cmd
python manage.py runserver
```

* Open localhost:8000